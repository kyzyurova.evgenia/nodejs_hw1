// requires...
const fs = require('fs');
const path = require('path');
// constants...

function createFile (req, res, next) {
  let dir = fs.readdirSync('./files', 'utf8', (err, data)=> data);
  // Your code to create the file.
  const possibleExt = ['.log', '.txt', '.js', '.json', '.yaml', '.xml'];
  try{
    let fileName = req.body.filename;
    let content = req.body.content;
    console.log(fileName, content); //console.log()
    if(dir.includes(fileName)){
      throw new Error('file is already exist');
    }
    if(fileName === undefined || content === undefined){
      throw new Error('filename or content is empty');
    }
    if(possibleExt.includes(path.extname(fileName))){
      fs.writeFileSync(`./files/${fileName}`, content, ()=>{});
      res.status(200).send({ "message": "File created successfully" });
      
    }else{
      throw new Error('wrong extention');
    }
    console.log(path.extname(fileName));
    
  }catch (err){
    console.error('user error');
    res.status(400).send({ "message": err.message });
  }
  
  
}

function getFiles (req, res, next) {
  // Your code to get all files.
  let dir = fs.readdirSync('./files', 'utf8', (err, data)=> data);
  res.status(200).send({
    "message": "Success",
    "files": dir});
}

const getFile = (req, res, next) => {
  // Your code to get all files.
  let dir = fs.readdirSync('./files', 'utf8', (err, data)=> data);
  try{
    let fileName = path.basename(req.url);
    if(dir.includes(fileName)){
      let content = fs.readFileSync(`./files/${fileName}`, 'utf8', (err, data)=> data);
      let ext = path.extname(fileName);
      res.status(200).send({
        "message": "Success",
        "filename": fileName,
        "content": content,
        "extension": ext.replace('.',''),
        "uploadedDate": "2017-07-22T17:32:28Z"});
    }else{
      throw new Error('wrong filename');
    }
    
  }catch (err){
    console.error('user error');
    res.status(400).send({ "message": err.message });
  }
  
}

// Other functions - editFile, deleteFile

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile
}
